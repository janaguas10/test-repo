import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class DataService {

  private dataUrl = '../assets/json/';

  constructor(private http: HttpClient) { }

  getJSON(data): Observable<any> {
    return this.http.get(this.dataUrl + data + '.json');
}

}
