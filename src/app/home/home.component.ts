import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(private dataService: DataService) { }

  membersList;
  categoryList;

  ngOnInit() {
    this.getMembers();
  }

  getMembers() {
    this.dataService.getJSON('members')
    .subscribe((data) => {
      if (data) {
        this.membersList = data;
      }
    },
      (err) => {
        console.log(err);
      })
  }
}
