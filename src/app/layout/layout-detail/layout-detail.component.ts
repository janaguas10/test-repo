import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-layout-detail',
  templateUrl: './layout-detail.component.html',
  styleUrls: ['./layout-detail.component.scss']
})
export class LayoutDetailComponent implements OnInit {

  constructor(private route: ActivatedRoute,
    private router: Router) { }

  categoryList;
  categoryNamesList;
  verifyRoute
  categoryData;

  ngOnInit() {
    this.route.params.subscribe((params) => {

      this.categoryList = JSON.parse(sessionStorage.getItem('categories'));
      this.categoryNamesList = this.categoryList.map(cat => cat.path);
      
      this.verifyRoute = this.categoryNamesList.find(cat => cat == params['page']); 
      this.verifyRoute ? this.getDataOfRoute(params['page']) : this.router.navigate(['/home']);
    })
  }

  getDataOfRoute(route) {
    this.categoryData = this.categoryList.find(cat => cat.path == route);
  }

}
