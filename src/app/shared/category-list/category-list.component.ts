import { Component, OnInit, Input } from '@angular/core';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-category-list',
  templateUrl: './category-list.component.html',
  styleUrls: ['./category-list.component.scss']
})
export class CategoryListComponent implements OnInit {
  constructor(private dataService: DataService) { }

  @Input() categoryList;

  ngOnInit() {
    sessionStorage.getItem('categories') ? this.categoryList = JSON.parse(sessionStorage.getItem('categories')) : this.getCategories();
  }

  getCategories() {
    this.dataService.getJSON('main-category')
      .subscribe((data) => {
        if (data) {
          this.categoryList = data;
          sessionStorage.setItem('categories', JSON.stringify(this.categoryList));
        }
      },
        (err) => {
          console.log(err);
        })
  }
}
